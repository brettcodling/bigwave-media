<?php

/**
 * @author Brett Codling
 * @copyright 19/07/2018
 * @phpversion 7.1.16
 */

namespace Bigwave\Ajax;

use Bigwave\Classes\Database;
use Bigwave\Classes\Helper;
use Bigwave\Objects\JsonParser;

require_once('../autoloader.php');

class getJson {

    public function __construct() {
        if (isset($_POST['json'])) {
            $parser = new JsonParser((new Database), (new Helper)->getJsonUrl($_POST['json']));
            echo json_encode($parser->getData());
            exit;
        }
    }

}

ignore_user_abort(0);
new getJson;