<?php

/**
 * @author Brett Codling
 * @copyright 19/07/2018
 * @phpversion 7.1.16
 */

session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// the tpl class sets up the smarty template engine
include_once($_SERVER['DOCUMENT_ROOT'].'/classes/tpl.class.php');

// auto loader to help with namespaces
spl_autoload_extensions('.object.php,.class.php');
spl_autoload_register(function ($className) {
	$className = str_replace("Bigwave", "", $className);
	set_include_path($_SERVER['DOCUMENT_ROOT']);
	spl_autoload($_SERVER['DOCUMENT_ROOT'].$className);
});