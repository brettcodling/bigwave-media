<?php

/**
 * @author Brett Codling
 * @copyright 20/07/2018
 * @phpversion 7.1.16
 */

namespace Bigwave\Classes;

use \PDO;

class Database {

    private $db;

    // set up Database class
    public function __construct() {
        $this->db = new PDO("mysql:host=localhost;dbname=bigwave", "root", "", [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING]);
    }

    // any functions called on the Database object send on to the PDO object
    public function __call($name, $arguments) {
        return call_user_func_array([$this->db, $name], $arguments);
    }

}