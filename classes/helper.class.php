<?php

/**
 * @author Brett Codling
 * @copyright 19/07/2018
 * @phpversion 7.1.16
 */

namespace Bigwave\Classes;

use Bigwave\Classes\Tpl;
use Bigwave\Classes\Database;

class Helper {

    var $db;
    
    // set up Helper class
    public function __construct($page = "Bigwave") {
        $this->connect();
        tpl::assign("page", $page);
    }

    // connect to db
    public function connect() {
        if (!$this->db) {
            $this->db = (new Database());
        }
    }

    // get the available json feed names
    public function getJsonNames() {
        $this->connect();
        $nameQuery = $this->db->query("SELECT name FROM json_urls");
        $names = [];
        while($name = $nameQuery->fetch()) {
            $names[] = $name->name;
        }
        // setting this as a default in case you dont fill a database
        if (!count($names)) {
            $names[] = "British Cycling";
        }
        return $names;
    }

    // get the url of the given json name
    public function getJsonUrl() {
        $json = $_POST['json'];
        $this->connect();
        $urlQuery = $this->db->prepare("SELECT url FROM json_urls WHERE name=:name OR url=:name");
        $urlQuery->execute(array('name' => $json));
        $url = $urlQuery->fetchColumn();
        if (!$url) {
            // if json name doesnt exist it must be a new one
            $url_parts = parse_url($json);
            if (isset($url_parts['host']) && $url_parts['host']) {
                $insert = $this->db->prepare("INSERT INTO json_urls (name, url) VALUES (:name, :url)");
                $insert->execute(["name" => $url_parts['host'], "url" => $json]);
                $url = $json;
                $_POST['json'] = $url_parts['host'];
            }
        }
        return $url;
    }

}