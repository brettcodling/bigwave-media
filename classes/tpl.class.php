<?php

/**
 * @author Brett Codling
 * @copyright 19/07/2018
 * @phpversion 7.1.16
 */

namespace Bigwave\Classes;

require_once(__DIR__.'/../smarty/Smarty.class.php');

// Set up smarty variables
if (!defined('SMARTY_DIR')){
	define('SMARTY_DIR', __DIR__.'/../smarty/');
}
$smarty = new \Smarty();
$smarty->setTemplateDir(__DIR__.'/../templates/');
$smarty->setCacheDir(__DIR__.'/../templates/cache/');
$smarty->setCompileDir(__DIR__.'/../templates/templates_c');
$smarty->error_reporting = 0;

class tpl {

	public static $smarty;

	// Assign a PHP variable to the smarty template
	public static function assign($var, $val) {
		self::$smarty->assign($var, $val);
	}

	// Assign multiple PHP variables at once
	public static function bulk_assign($tpl_vars) {
		foreach ($tpl_vars as $var => $val) {
			self::assign($var, $val);
		}
	}

	// display the smarty template page
	public static function display($tpl) { 
		if (!self::$smarty) {
			__construct();
		}
		if (!preg_match('/\.tpl$/', $tpl)){
			$tpl .= '.tpl';
		}
		self::$smarty->display($tpl);
	}

	// Assign the smarty object to a static variable
	public static function setSmarty() { global $smarty;
		self::$smarty = $smarty;
	}
}

tpl::setSmarty();