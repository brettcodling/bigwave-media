<?php
/**
 * @author Brett Codling
 * @copyright 19/07/2018
 * @phpversion 7.1.16
 */

namespace Bigwave;

use Bigwave\Classes\Tpl;
use Bigwave\Classes\Helper;

require_once('autoloader.php');

class index extends Helper {

    public function __construct() {
        // construct the helper class
        parent::__construct();
        tpl::assign('names', $this->getJsonNames());
        // display index page
        tpl::display('index');
    }

}

new index;