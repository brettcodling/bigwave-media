var latestRequest;
var hiddenSearch = [];
var hiddenDate = [];
var markers;
$(document).ready(function(){

    console.log("main.js");

    /* init date pickers */
    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd"
    });

    /* when go is clicked next to new url input submit the form with select disabled */
    $(document).on('click', '.newurl', function(){
        if ($('.newurlinput').val().length > 0) {
            $('select').attr('disabled', true);
            $('form').submit();
        }
    });

    /* perform date filter */
    $(document).on('click', '#dateSearch', function(){
        var start = $('#start').val()?parseInt($('#start').val().replace(/\-/g, '')):false;
        var end = $('#end').val()?parseInt($('#end').val().replace(/\-/g, '')):false;
        $.each($('.event'), function(i,v) {
            var date = parseInt($(this).attr('data-date').substr(0, 8));
            var eventid = $(this).data('event');
            if ((!start && !end) || ((!start || date >= start) && (!end || date <= end))) {
                /* show events */
                if ($(this).hasClass('datehide')) {
                    $(this).removeClass('datehide');
                    if ($.inArray(eventid, hiddenDate) > -1) {
                        hiddenDate.splice($.inArray(eventid, hiddenDate), 1);
                    }
                    if (markers && $.inArray(eventid, hiddenSearch) === -1) {
                        markers[eventid].setVisible(true);
                    }
                }
            } else {
                /* hide events */
                if (!$(this).hasClass('datehide')) {
                    $(this).addClass('datehide');
                    hiddenDate.push(eventid);
                    if (markers) {
                        markers[eventid].setVisible(false);
                    }
                }
            }
        });
    });

    /* on change of select submit form */
    $(document).on('change', 'select[name="json"]', function(){
        $('.newurlinput').attr('disabled', true);
        $('#jsonForm').submit();
    });

    /* change between map and list view */
    $(document).on('click', '.change-view:not(.active)', function(){
        var tab = $('#'+$(this).data('view'));
        tab.removeClass('d-none');
        $('.tab').not(tab).addClass('d-none');
        $('.change-view').removeClass('active');
        $(this).addClass('active');
    });

    /* perform search on keyup of search box */
    $(document).on('keyup', '#search', function(){
        var search = $(this).val();
        var pattern = new RegExp(search, "i");
        $.each($('.event'), function(i,v){
            var text = $(this).find('.title').html()+
            $(this).find('.description').html()+
            $(this).find('.date').html()+
            $(this).find('address').html();
            var eventid = $(this).data('event');
            if (pattern.test(text.replace(/\<br\>/g, '')) || search.length < 1){
                /* show events */
                if (!$(this).hasClass('d-flex')) {
                    $(this).removeClass('d-none').addClass('d-flex');
                    if ($.inArray(eventid, hiddenSearch) > -1) {
                        hiddenSearch.splice($.inArray(eventid, hiddenSearch), 1);
                    }
                    if (markers && $.inArray(eventid, hiddenDate) === -1) {
                        markers[eventid].setVisible(true);
                    }
                }
            } else {
                /* hide events */
                if (!$(this).hasClass('d-none')) {
                    $(this).addClass('d-none').removeClass('d-flex');
                    hiddenSearch.push(eventid);
                    if (markers) {
                        markers[eventid].setVisible(false);
                    }
                }
            }
        });
    });

    /* refresh data every 30 seconds */
    setInterval(function(){
        latestRequest = $.post('/ajax/getJson.php', {json: $('select[name="json"]').val()}, function(output){
            var data = $.parseJSON(output);
            var eventContainer;
            $.each(data, function(i,v) {
                /* set data */
                eventContainer = $('.event[data-event="'+$(this).id+'"]');
                eventContainer.attr('data-date', $(this).date);
                eventContainer.find('.title').html($(this).title);
                eventContainer.find('.description').html($(this).desription);
                eventContainer.find('.date').html($(this).formatted_date);
                eventContainer.find('address').html(
                    ($(this).street_address ? $(this).street_address+"<br>" : "") + 
                    ($(this).address_region ? $(this).address_region+"<br>" : "") + 
                    ($(this).postal_code ? $(this).postal_code+"<br>" : "") + 
                    ($(this).address_country ? $(this).address_country : "")
                );
                if ($(this).url) {
                    eventContainer.find('.urlContainer').html('<a href="'+$(this).url+'" class="ml-auto">'+$(this).url+'</a>');
                }
            });
            performMap(data);
        });
    }, 30000);

});

/* initialise google map */
function initMap() {
    if (latestRequest) {
        latestRequest.abort();
    }
    latestRequest = $.post('/ajax/getJson.php', {json: $('select[name="json"]').val()}, function(output){
        var data = $.parseJSON(output);
        performMap(data);
    });
}

/* create map, markers and infowindows */
function performMap(data) {
    var bigwave = {lat: 50.7198449, lng: -3.5365653};
    var map = new google.maps.Map(document.getElementById('map'), {zoom: 4, center: bigwave});
    markers = {};
    $.each(data, function(i,v) {
        if (v.lat && v.lng) {
            var location = {lat: parseFloat(v.lat), lng: parseFloat(v.lng)};
            var contentString = '<div class="card p-3 d-flex event" data-event="'+v.id+'" data-date="'+v.date+'">' +
            '<h5 class="border-bottom d-flex"><span class="title">'+v.title+'</span>' +
            '<span class="ml-auto date">'+v.formatted_date+'</span></h5>' +
            '<p><img src="'+v.thumbnail+'" alt="No Thumbnail Available." class="img-thumbnail bg-secondary">' +
            '<span class="description">'+(v.description?v.description:"No Description Available.")+'</span>' +
            '</p><div class="row"><div class="col-md-6 col-12">' +
            '<b>Address</b><address class="mr-auto">';
            if (v.street_address) {
                contentString += v.street_address+"<br>";
            }
            if (v.address_region) {
                contentString += v.address_region+"<br>";
            }
            if (v.postal_code) {
                contentString += v.postal_code+"<br>";
            }
            if (v.address_country) {
                contentString += v.address_country+"<br>";
            }
            contentString += '</address></div><div class="col-md-6 my-auto text-md-right col-12 urlContainer">' +
            (v.url ? '<a href="'+v.url+'" class="ml-auto">'+v.url+'</a>' : '')+'</div></div></div>';
            var infowindow = new google.maps.InfoWindow({content: contentString});
            var marker = (new google.maps.Marker({position: location, map: map}));
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
            markers[v.id] = marker;
        }
    });
    $.each(hiddenSearch, function(i,v){
        markers[v].setVisible(false);
    });
    $.each(hiddenDate, function(i,v){
        markers[v].setVisible(false);
    });
}