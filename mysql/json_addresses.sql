-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 24, 2018 at 10:09 PM
-- Server version: 5.7.21
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bigwave`
--

-- --------------------------------------------------------

--
-- Table structure for table `json_addresses`
--

DROP TABLE IF EXISTS `json_addresses`;
CREATE TABLE IF NOT EXISTS `json_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `json_item_id` int(11) NOT NULL,
  `street_address` varchar(255) NOT NULL,
  `address_locality` varchar(255) NOT NULL,
  `address_region` varchar(255) NOT NULL,
  `postal_code` varchar(20) NOT NULL,
  `address_country` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `json_item_id` (`json_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
