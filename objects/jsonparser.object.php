<?php

/**
 * @author Brett Codling
 * @copyright 19/07/2018
 * @phpversion 7.1.16
 */

namespace Bigwave\Objects;

class JsonParser {

    private $data = [],
            $db,
            $storeAdd,
            $storeItem,
            $url,
            $urlId;

    // set up the JsonParser object
    public function __construct($db, $url) {
        $this->db = $db;
        $this->url = $url;
        $this->getStoredData();
    }

    // provide default value
    private function defaultVal($item, $value, $default) {
        if (isset($item->$value)) {
            return $item->$value;
        } else {
            return $default;
        }
    }

    // return the private data
    public function getData() {
        return $this->data;
    }

    // check if there is any up to date data
    private function getStoredData() {
        $urlIdQuery = $this->db->prepare("SELECT id FROM json_urls WHERE url=:url");
        $urlIdQuery->execute(["url" => $this->url]);
        $this->urlId = $urlIdQuery->fetchColumn();
        $dataQuery = $this->db->prepare("SELECT ji.id, title, description, DATE_FORMAT(date, '%d %M %Y') AS formatted_date, date, thumbnail, url, lat, lng, street_address, address_locality, address_region, postal_code, address_country FROM json_items AS ji INNER JOIN json_addresses AS ja ON ji.id=ja.json_item_id WHERE ji.json_url_id=:url_id AND ji.inserted > :10MinutesAgo ORDER BY ji.date DESC");
        $dataQuery->execute(["url_id" => $this->urlId, "10MinutesAgo" => date('YmdHis', strtotime('- 10 minutes'))]);
        while($data = $dataQuery->fetch()) {
            $this->data[] = $data;
        }
        // if there is no up to date data then process the json feed
        if (!count($this->data)) {
            $deleteQuery = $this->db->prepare("DELETE ji, ja FROM json_items AS ji INNER JOIN json_addresses AS ja ON ja.json_item_id=ji.id WHERE ji.json_url_id=:json_url_id");
            $deleteQuery->execute(["json_url_id" => $this->urlId]);
            $curlResult = $this->performCurl();
            if (isset($curlResult->items)) {
                foreach($curlResult->items as $item) {
                    if (isset($item->data)) {
                        $this->storeItem($item->data);
                    }
                }
                $dataQuery->execute(["url_id" => $this->urlId, "10MinutesAgo" => date('YmdHis', strtotime('- 10 minutes'))]);
                while($data = $dataQuery->fetch()) {
                    $this->data[] = $data;
                }
            }
        }
    }

    // curl the json feed and decode the response
    private function performCurl() {
        $curl = curl_init($this->url);
	    curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        return json_decode(curl_exec($curl));
    }

    // store to the database
    private function storeItem($item) {
        if (!$this->storeItem) {
            $this->storeItem = $this->db->prepare("INSERT INTO json_items (json_url_id, title, description, `date`, thumbnail, url, lat, lng, inserted) VALUES (:json_url_id, :title, :description, :date, :thumbnail, :url, :lat, :lng, :inserted)");
        }
        if (!$this->storeAdd) {
            $this->storeAdd = $this->db->prepare("INSERT INTO json_addresses (json_item_id, street_address, address_locality, address_region, postal_code, address_country) VALUES (:json_item_id, :street_address, :address_locality, :address_region, :postal_code, :address_country)");
        }
        $this->storeItem->execute([
            "json_url_id"   => $this->urlId,
            "title"         => $this->defaultVal($item, "name", ""),
            "description"   => $this->defaultVal($item, "description", ""),
            "date"          => isset($item->startDate) ? date('YmdHis', strtotime($item->startDate)) : "",
            "thumbnail"     => "",// these dont exist so i have no idea how to process the data...
            "url"           => $this->defaultVal($item, "url", ""),
            "lat"           => isset($item->location->geo->latitude) ? $item->location->geo->latitude : 0,
            "lng"           => isset($item->location->geo->longitude) ? $item->location->geo->longitude : 0,
            "inserted"      => date('YmdHis')
        ]);
        $this->storeAdd->execute([
            "json_item_id"      => $this->db->lastInsertId(),
            "street_address"    => isset($item->location->address->streetAddress) ? $item->location->address->streetAddress : "",
            "address_locality"  => isset($item->location->address->addressLocality) ? $item->location->address->addressLocality : "",
            "address_region"    => isset($item->location->address->addressRegion) ? $item->location->address->addressRegion : "",
            "postal_code"       => isset($item->location->address->postalCode) ? $item->location->address->postalCode : "",
            "postal_code"       => isset($item->location->address->postalCode) ? $item->location->address->postalCode : "",
            "address_country"   => isset($item->location->address->addressCountry) ? $item->location->address->addressCountry : ""
        ]);
    }

}