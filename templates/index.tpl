{extends file="header.tpl"}
{block "content"}
    <div class="container-fluid h-100 d-flex">
        <div class="row">
            <div class="col-10 col-md-6 m-auto">
                <img src="/images/bigwave_logo.jpg" class="w-100">
                <p class="text-center">
                    Select a JSON feed from below to parse it
                </p>
                <form method="post" action="view" id="jsonForm">
                    <select class="form-control mb-2" name="json">
                        <option selected disabled>Please select a json feed...</option>
                        {foreach from=$names item=name}
                            <option>{$name}</option>
                        {/foreach}
                    </select>
                    <div class="row text-center">
                        <span class="mx-auto">Or</span>
                    </div>
                    <div class="row mt-2">
                        <div class="col-10">
                            <input class="form-control newurlinput" name="json" placeholder="Enter a new URL...">
                        </div>
                        <div class="col-2">
                            <button class="btn btn-info newurl" type="button">Go</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
{/block}