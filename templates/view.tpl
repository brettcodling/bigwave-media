{extends file="header.tpl"}
{block "content"}
    <nav class="navbar mb-auto navbar-light bg-light my-navbar">
        <div class="w-100">
            <div class="row">
                <div class="d-none d-md-flex col-md-4 justify-content-start my-2">
                    <a class="navbar-brand" href="/">Bigwave</a>
                </div>
                <div class="col-12 col-md-4 my-2">
                    <form method="post" action="view" id="jsonForm" class="form-inline m-0 d-flex justify-content-center">
                        <span>Select a different json feed&nbsp;</span>
                        <select class="form-control" name="json">
                            {foreach from=$names item=name}
                                <option{if $name|lower == $selectedName|lower} selected{/if}>{$name}</option>
                            {/foreach}
                        </select>
                    </form>
                </div>
                <div class="col-12 col-md-4 text-center text-md-right my-2">
                    <div class="btn-group">
                        <button class="btn btn-info change-view active" data-view="list">List View</button>
                        <button class="btn btn-info change-view" data-view="map">Map View</button>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="container mt-auto">
        <h3 class="text-center my-2">{$selectedName|ucwords} Events</h3>
        <div class="row">
            <div class="col-12 col-md-6 mr-auto mb-3">
                <div class="row">
                    <div class="col-5"><input id="start" class="datepicker form-control" placeholder="Date from..."></div>
                    <div class="col-5"><input id="end" class="datepicker form-control" placeholder="Date to..."></div>
                    <div class="col-2 pl-0 pl-md-3 text-center"><button class="btn btn-info" id="dateSearch">Go</button></div>
                </div>
            </div>
            <div class="col-12 col-md-3 ml-auto mb-3">
                <input class="form-control" placeholder="Search..." id="search">
            </div>
        </div>
        <div class="tab" id="list">
            {foreach from=$data item=event}
                <div class="card p-3 mb-3 d-flex event" data-event="{$event->id}" data-date="{$event->date}">
                    <h5 class="border-bottom d-flex"><span class="title">{$event->title}</span><span class="ml-auto date">{$event->formatted_date}</span></h5>
                    <p>
                        <img src="{$event->thumbnail}" alt="No Thumbnail Available." class="img-thumbnail bg-secondary">
                        <span class="description">{$event->description|default:"No Description Available."}</span>
                    </p>
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <b>Address</b>
                            <address class="mr-auto">
                                {if $event->street_address}{$event->street_address}<br>{/if}
                                {if $event->address_region}{$event->address_region}<br>{/if}
                                {if $event->postal_code}{$event->postal_code}<br>{/if}
                                {if $event->address_country}{$event->address_country}{/if}
                            </address>
                        </div>
                        <div class="col-md-6 my-auto text-md-right col-12 urlContainer">
                            {if $event->url}<a href="{$event->url}" class="ml-auto">{$event->url}</a>{/if}
                        </div>
                    </div>
                </div>
            {/foreach}
        </div>
        <div class="tab d-none" id="map"></div>
    </div>
{/block}