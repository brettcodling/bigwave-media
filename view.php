<?php

/**
 * @author Brett Codling
 * @copyright 19/07/2018
 * @phpversion 7.1.16
 */

namespace Bigwave;

use Bigwave\Classes\Tpl;
use Bigwave\Classes\Helper;
use Bigwave\Objects\JsonParser;

require_once('autoloader.php');

class View extends Helper {

    public function __construct() {
        // check for a json identifier
        if (!isset($_POST['json'])) {
            header('Location: /');
        }
        // construct the ~Helper class
        parent::__construct();
        $parser = $this->connectToJson();
        tpl::bulk_assign([
            "selectedName" => $_POST['json'],
            "data" => $parser->getData(),
            "names" => $this->getJsonNames(),
            "api_key" => "AIzaSyCbMUFwXEYj4IlFsZ8zbYQf2D-IZy-YjgM"
        ]);
        // display view page
        tpl::display('view');
    }

    private function connectToJson() {
        // get the url of the json feed
        $url = $this->getJsonUrl();
        if (!$url) {
            header('Location: /');
        }
        // set up the JsonParser object
        $parser = new JsonParser($this->db, $url);
        return $parser;
    }

}

new View;